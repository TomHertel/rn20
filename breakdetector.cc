/*
 * Break Detector
 * Copyright (C) 2020 by Thomas Dreibholz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact: dreibh@simula.no
 */

#include "breakdetector.h"

#include <signal.h>

#include <atomic>
#include <cstdio>
#include <iostream>


// ###### Global variables ##################################################
static std::atomic<bool> DetectedBreak(false);
static bool              PrintedBreak = false;

// Mutex for thread-safe usage of std::cout/std::cerr
std::mutex               IOMutex;


// ###### Handler for SIGINT ################################################
static void breakDetector(int signum)
{
   DetectedBreak = true;
}


// ###### Install break detector ############################################
void installBreakDetector()
{
   DetectedBreak = false;
   PrintedBreak  = false;
   signal(SIGINT, &breakDetector);
}


// ###### Unnstall break detector ###########################################
void uninstallBreakDetector()
{
   signal(SIGINT, SIG_DFL);
   PrintedBreak  = false;
   DetectedBreak = false;
}


// ###### Check, if break has been detected #################################
bool breakDetected()
{
   if((DetectedBreak) && (!PrintedBreak)) {
      std::lock_guard<std::mutex> lock(IOMutex);
      std::cerr << "\n*** Break ***    Signal #" << SIGINT << "\n\n";
      PrintedBreak = true;
   }
   return(DetectedBreak);
}
