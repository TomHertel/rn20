/*
 * Echo Server, poll() version
 * Copyright (C) 2020 by Thomas Dreibholz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact: dreibh@simula.no
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <iostream>
#include <set>

#include "breakdetector.h"


// ##### Echo service #######################################################
bool performApplication(int sd)
{
   char    buffer[1024];
   ssize_t bytesRead;

   if( (bytesRead = recv(sd, (char*)&buffer, sizeof(buffer), 0)) > 0) {
      std::cout << "Received " << bytesRead << " from SD " << sd << std::endl;
      if(send(sd, (char*)&buffer, bytesRead, MSG_NOSIGNAL|MSG_DONTWAIT) > 0) {
         return true;
      }
      std::cout << "Failed to send on SD " << sd << std::endl;
   }

   // ====== Shutdown connection =========================================
   std::cout << "Closing SD " << sd << std::endl;
   close(sd);
   return false;
}


// ###### Main program ######################################################
int main(int argc, char** argv)
{
   if(argc < 2) {
      std::cerr << "Usage: " << argv[0] << " [Port]" << std::endl;
      exit(1);
   }
   const char* localService = argv[1];

   // ====== Get local address (resolve hostname and service) ===============
   struct addrinfo* ainfo = NULL;
   struct addrinfo  ainfohint;
   memset((char*)&ainfohint, 0, sizeof(ainfohint));
   // AI_PASSIVE will set address to the ANY address.
   ainfohint.ai_flags    = AI_PASSIVE;
   ainfohint.ai_family   = AF_UNSPEC;
   ainfohint.ai_socktype = SOCK_STREAM;
   ainfohint.ai_protocol = IPPROTO_TCP;
   int error = getaddrinfo(NULL, localService, &ainfohint, &ainfo);
   if(error != 0) {
      std::cerr << "ERROR: getaddrinfo() failed: " << gai_strerror(error) << std::endl;
      exit(1);
   }

   // ====== Convert local address to human-readable format =================
   char resolvedHost[NI_MAXHOST];
   char resolvedService[NI_MAXSERV];
   error = getnameinfo(ainfo->ai_addr, ainfo->ai_addrlen,
                       (char*)&resolvedHost, sizeof(resolvedHost),
                       (char*)&resolvedService, sizeof(resolvedService),
                       NI_NUMERICHOST);
   if(error != 0) {
      std::cerr << "ERROR: getnameinfo() failed: " << gai_strerror(error) << std::endl;
      exit(1);
   }
   std::cout << "Binding to local host "
             << resolvedHost << ", service " << resolvedService << " ..." << std::endl;

   // ====== Create socket of appropriate type ==============================
   int sd = socket(ainfo->ai_family, ainfo->ai_socktype, ainfo->ai_protocol);
   if(sd <= 0) {
      perror("Unable to create socket");
      exit(1);
   }

   // ====== Bind to local port ==============================================
   if(bind(sd, ainfo->ai_addr, ainfo->ai_addrlen) < 0) {
      perror("bind() call failed");
      exit(1);
   }

   // ====== Turn socket into "listen" mode =================================
   if(listen(sd, 10) < 0) {
      perror("listen() call failed!");
   }


   // ====== Main loop ======================================================
   std::set<int> socketSet;

   installBreakDetector();
   while(!breakDetected()) {
      // ====== Wait for events =============================================
      const size_t connections = socketSet.size();

      pollfd pfd[1 + connections];
      pfd[0].fd     = sd;
      pfd[0].events = POLLIN;

      int i = 1;
      for(auto iterator = socketSet.begin(); iterator != socketSet.end(); iterator++) {
         pfd[i].fd     = *iterator;
         pfd[i].events = POLLIN;
         i++;
      }
      int events = poll((pollfd*)&pfd, 1 + connections, -1);

      // ====== Handle events ===============================================
      if(events > 0) {
         // ====== Accept incoming connection ===============================
         if(pfd[0].revents & POLLIN) {
            sockaddr_storage remoteAddress;
            socklen_t        remoteAddressLength = sizeof(remoteAddress);
            int newSD = accept(sd, (sockaddr*)&remoteAddress, &remoteAddressLength);
            if(newSD < 0) {
               perror("accept() call failed");
               break;
            }

            socketSet.insert(newSD);
         }

         // ====== Handle incoming data on a connection =====================
         for(size_t i = 1; i <= connections; i++) {
            if(pfd[i].revents & POLLIN) {
               // ====== Perform application ================================
               if(performApplication(pfd[i].fd) == false) {
                  socketSet.erase(pfd[i].fd);   // Connection has been closed!
               }
            }
         }
      }
   }
   uninstallBreakDetector();


   // ====== Clean up =======================================================
   auto iterator = socketSet.begin();
   while(iterator != socketSet.end()) {
      close(*iterator);
      socketSet.erase(iterator);
      iterator = socketSet.begin();
   }
   freeaddrinfo(ainfo);
   close(sd);
   return 0;
}
