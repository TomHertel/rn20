/*
 * Echo Server, multi-threaded version
 * Copyright (C) 2020 by Thomas Dreibholz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact: dreibh@simula.no
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <atomic>
#include <cassert>
#include <iostream>
#include <mutex>
#include <set>
#include <thread>

#include "breakdetector.h"


struct WorkerThread {
   std::thread       thread;
   int               sd;
   std::atomic<bool> finished;
};


// ##### Echo service #######################################################
void performApplication(WorkerThread* thread, int sd)
{
   char    buffer[1024];
   ssize_t bytesRead;

   while( (bytesRead = recv(sd, (char*)&buffer, sizeof(buffer), 0)) > 0) {
      std::lock_guard<std::mutex> lock(IOMutex);
      std::cout << "Received " << bytesRead << " from SD " << sd << std::endl;
      if(send(sd, (char*)&buffer, bytesRead, MSG_NOSIGNAL) < 0) {
         std::cout << "Failed to send on SD " << sd << std::endl;
         break;
      }
   }

   // ====== Shutdown connection =========================================
   IOMutex.lock();
   std::cout << "Closing SD " << sd << std::endl;
   IOMutex.unlock();
   close(sd);
   thread->finished.exchange(true);
}


// ###### Main program ######################################################
int main(int argc, char** argv)
{
   if(argc < 2) {
      std::cerr << "Usage: " << argv[0] << " [Port]" << std::endl;
      exit(1);
   }
   const char* localService = argv[1];

   // ====== Get local address (resolve hostname and service) ===============
   struct addrinfo* ainfo = NULL;
   struct addrinfo  ainfohint;
   memset((char*)&ainfohint, 0, sizeof(ainfohint));
   // AI_PASSIVE will set address to the ANY address.
   ainfohint.ai_flags    = AI_PASSIVE;
   ainfohint.ai_family   = AF_UNSPEC;
   ainfohint.ai_socktype = SOCK_STREAM;
   ainfohint.ai_protocol = IPPROTO_TCP;
   int error = getaddrinfo(NULL, localService, &ainfohint, &ainfo);
   if(error != 0) {
      std::cerr << "ERROR: getaddrinfo() failed: " << gai_strerror(error) << std::endl;
      exit(1);
   }

   // ====== Convert local address to human-readable format =================
   char resolvedHost[NI_MAXHOST];
   char resolvedService[NI_MAXSERV];
   error = getnameinfo(ainfo->ai_addr, ainfo->ai_addrlen,
                       (char*)&resolvedHost, sizeof(resolvedHost),
                       (char*)&resolvedService, sizeof(resolvedService),
                       NI_NUMERICHOST);
   if(error != 0) {
      std::cerr << "ERROR: getnameinfo() failed: " << gai_strerror(error) << std::endl;
      exit(1);
   }
   std::cout << "Binding to local host "
             << resolvedHost << ", service " << resolvedService << " ..." << std::endl;

   // ====== Create socket of appropriate type ==============================
   int sd = socket(ainfo->ai_family, ainfo->ai_socktype, ainfo->ai_protocol);
   if(sd <= 0) {
      perror("Unable to create socket");
      exit(1);
   }

   // ====== Bind to local port =============================================
   if(bind(sd, ainfo->ai_addr, ainfo->ai_addrlen) < 0) {
      perror("bind() call failed");
      exit(1);
   }

   // ====== Turn socket into "listen" mode =================================
   if(listen(sd, 10) < 0) {
      perror("listen() call failed!");
   }


   // ====== Main loop ======================================================
   std::set<WorkerThread*> threadSet;

   installBreakDetector();
   while(!breakDetected()) {
      // ====== Wait for events =============================================
      pollfd pfd[1];
      pfd[0].fd     = sd;
      pfd[0].events = POLLIN;
      int events = poll((pollfd*)&pfd, 1, -1);

      // ====== Handle events ===============================================
      if(events > 0) {

         // ====== Accept incoming connection ===============================
         sockaddr_storage remoteAddress;
         socklen_t        remoteAddressLength = sizeof(remoteAddress);
         int newSD = accept(sd, (sockaddr*)&remoteAddress, &remoteAddressLength);
         if(newSD < 0) {
            perror("accept() call failed");
            break;
         }

         // ====== Perform application ======================================
         WorkerThread* newThread = new WorkerThread;
         assert(newThread != nullptr);
         newThread->finished = false;
         newThread->sd       = newSD;
         newThread->thread   = std::thread(performApplication, newThread, newSD);
         threadSet.insert(newThread);

         IOMutex.lock();
         std::cout << "Created thread for SD " << newThread->sd << std::endl;
         IOMutex.unlock();

         // ====== Garbage collection =======================================
         auto iterator = threadSet.begin();
         while(iterator != threadSet.end()) {
            WorkerThread* currentThread = *iterator;
            iterator++;

            if(currentThread->finished) {
               IOMutex.lock();
               std::cout << "Removing thread for SD " << currentThread->sd << std::endl;
               IOMutex.unlock();
               currentThread->thread.join();
               threadSet.erase(currentThread);
               delete currentThread;
            }
         }
      }
   }
   uninstallBreakDetector();


   // ====== Clean up =======================================================
   auto iterator = threadSet.begin();
   while(iterator != threadSet.end()) {
      WorkerThread* currentThread = *iterator;
      IOMutex.lock();
      std::cout << "Removing thread for SD " << currentThread->sd << std::endl;
      IOMutex.unlock();
      shutdown(currentThread->sd, SHUT_RDWR);   // Make sure the connection shut down!
      currentThread->thread.join();
      threadSet.erase(currentThread);
      delete currentThread;
      iterator = threadSet.begin();
   }
   freeaddrinfo(ainfo);
   close(sd);
   return 0;
}
