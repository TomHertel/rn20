#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <dirent.h>
#include <map>
#include <sys/stat.h>

#include <atomic>
#include <cassert>
#include <iostream>
#include <mutex>
#include <set>
#include <thread>
#include <string.h>
#include "breakdetector.h"
#include <arpa/inet.h>
#include <chrono>
#include <ctime>
#include <semaphore.h>

using namespace std;

struct ranges {
	int badContentRange;
	string upperRange;
	string lowerRange;
	string rangeDummy;
	int isRangeSpecified;
};

struct requestReturns {
	char* buffer;
	string request;
};

string baseDir;

/*
 *  Definition und Behandlung von Ranges, falls dieses gesetzt wurde. Ablegung in einem struct zur besseren UEbergabe.
 */
ranges* rangeDefiner(string Request) {
	int isRangeSpecified = 0;
	int rangePos = 0;
	string request = Request;
	string fullRange("0");
	string lowerRange("0");
	string upperRange("-1");
	string rangeDummy("Range: bytes=");
	rangePos = request.find(rangeDummy);
	int badContentRange = 0;
	// Content Range prüfen
	if (rangePos > 0) {
		cout << "range erkannt" << endl;
		isRangeSpecified = 1;
		int firstNumberPos = rangePos + rangeDummy.length();
		int rangeSep = request.find('-', firstNumberPos);
		int len = rangeSep - firstNumberPos;
		lowerRange = request.substr(firstNumberPos, len);

		int nextBlankRange = request.find('\r', rangeSep);

		int len2 = nextBlankRange - rangeSep - 1;
		cout << len2 << endl;
		if (len2 > 0) {
			upperRange = request.substr(rangeSep + 1, len2);
			if (stoi(lowerRange) > stoi(upperRange)) {
				badContentRange = 1;
			}

		}
		cout << "UpperRange " << upperRange << endl;

	}
	ranges* structRanges = new ranges;
	structRanges->badContentRange = badContentRange;
	structRanges->upperRange = upperRange;
	structRanges->lowerRange = lowerRange;
	structRanges->rangeDummy = rangeDummy;
	structRanges->isRangeSpecified = isRangeSpecified;
	return structRanges;

}

/*
 *  Definition und Behandlung von Requests, die durch Clients gegeben werden. Ablegung in einem struct zur besseren UEbergabe.
 */
requestReturns* getRequest(int Sd) {
	char buffer[1024];
	ssize_t bytesRead;
	string request("");
	int sd = Sd;
	while ((bytesRead = recv(sd, (char*) &buffer, sizeof(buffer), 0)) > 0) {
		lock_guard<mutex> lock(IOMutex);
		cout << "Received " << bytesRead << " from SD " << sd << endl;
		request.append(buffer);
		for (int i = 0; i < 1024; i++) {
			buffer[i] = '\0';
		}
		if (request.find(string("\r\n\r\n")) != string::npos) {

			break;
		}

	}
	cout << "|" << request << "|" << endl;
	requestReturns* structRequestReturns = new requestReturns;
	structRequestReturns->buffer = buffer;
	structRequestReturns->request = request;
	return structRequestReturns;
}

struct WorkerThread {
	std::thread thread;
	int sd;
	atomic<bool> finished;
};

/*
 * Mapping der  Datentypen, die von dem Server bereitgestellt und benutzt werden sollen.
 */
map<string, string> MIME{{".html", "application/xhtml+xml"},
                         {".htm",  "application/xhtml+xml"},
                         {".css",  "text/css"},
                         {".jpeg", "image/jpeg"},
                         {".jpg",  "image/jpeg"},
                         {".png",  "image/png"},
                         {".webp", "image/webp"},
                         {".xml",  "application/xml"},
                         {".odt",  "application/vnd.oasis.opendocument.text"},
                         {".bib",  "text/x-bibtex"},
                         {".doc",  "application/msword"},
                         {".exe",  "application/octet-stream"},
                         {".sh",   "application/x-sh"},
                         {".js",   "application/javascript"},
                         {".pdf",  "application/pdf"},
                         {".svg",  "image/svg+xml"}
};

std::mutex writeLogfile;

/*
 * Funktionen die von den Threads ausgefuehrt werden soll, wie Fehlerbehandlung, Logging
 * das Einsetzen der festgelegten Range, Dateityp-Abgleich, Datenverarbeitung,
 * also Request-Anfragen von Clients erhalten und die entsprechenden Dateien senden.
 * Ist das von einem Client angefragte Ziel ein Verzeichnis, und existiert keine Datei index.html in
 * diesem Verzeichnis, so wird ein Directory Listing als HTML-Datei zurückgeliefert.
 * Existiert jedoch eine Datei index.html im Verzeichnis, wird diese zurückgeliefert.
 * Zugriffe und Fehler werden in der Logdatei mit Datum, Uhrzeit, IP-Adresse des Clients und
 * angefragte Datei/Verzeichnis + Ergebnis bzw. Fehlermeldung festgehalten.
 */

// ##### Echo service #######################################################
void performApplication(WorkerThread* thread, int sd, in_addr ipAddr, const char* logFileName) {
	int keepAlive = 0;
	do {
		FILE* logFile;
		string logString("");
		char ip[INET_ADDRSTRLEN];
		inet_ntop(AF_INET, &ipAddr, ip, INET_ADDRSTRLEN);
		cout << "IP verbunden: " << ip << endl;
		logString.append(ip);
		logString += " ";
		auto zeit = chrono::system_clock::now();
		auto zeit2 = chrono::system_clock::to_time_t(zeit);
		logString.append(ctime(&zeit2));
		logString += " ";
		requestReturns* structRequestReturns = getRequest(sd);
		string request = structRequestReturns->request;
		if (request.length() == 0) {
			break;
		}
		keepAlive = 0;

		ranges* structRanges = rangeDefiner(request);

		//  Datei erkennen
		int nextBlankPath = request.find(' ', 4);
		string pathstr = request.substr(4, nextBlankPath - 4);
		const char* path = pathstr.c_str();
		logString.append(path);
		logString += " ";
		if (path[0] == '/') {
			pathstr = "." + pathstr;
			path = pathstr.c_str();
		}
		auto finalPath = baseDir + path;
		path = finalPath.c_str();

      /*
       * Fehlerbehandlung. Im Fehlerfall gibt der Webserver einen sinnvollen Fehler zurück (z.B. 404 → Bad Request).
       */

		if (request.find(string("GET ")) == string::npos || request.find(string("GET ")) != 0) {
			cout << "404 Bad Request" << endl;
			logString += " 404 Bad Request ";
		} else if (structRanges->badContentRange == 1) {
			logString += " 404 Bad Content Range ";
			string err("404 Bad Content Range");
			cout << err << endl;
			if (send(sd, err.c_str(), err.length(), 0) < 0) {
				cout << "Failed to send on SD " << sd << endl;

			}

		} else if ((pathstr.find(string("..")) != string::npos)) {
			string err("404 Bad Path");
			logString += " 404 Bad Path ";
			cout << err << endl;
			if (send(sd, err.c_str(), err.length(), 0) < 0) {
				cout << "Failed to send on SD " << sd << endl;

			}

		} else {

			FILE* requestedFile;
			string mimeType;
			int endungPos = 0;

			/*
          * Behandlung von einzelnen Files, die requestet werden
          */
			if ((requestedFile = fopen(path, "r+")) != NULL) {
				cout << "File wurde geöffnet: " << path << endl;
				if ((endungPos = pathstr.find('.', 2)) != 0) {
					string endung = pathstr.substr(endungPos);
					auto search = MIME.find(endung);
					mimeType = search->second;

				} else {
					cout << "Kein Datei Typ erkannt" << endl;
				}

				char c = 'a';
				string out;

				fseek(requestedFile, 0, SEEK_END);
				int fileEnd = ftell(requestedFile);
				//cout << "file end " << fileEnd << endl;
				fseek(requestedFile, 0, SEEK_SET);
				if (!structRanges->isRangeSpecified) {

					for (int i = 0; i < fileEnd; i = ftell(requestedFile)) {
						c = fgetc(requestedFile);
						out += c;
					}
					int outLength = out.length();
					cout << "fülle buffer" << endl;
					char buf[1000];
					sprintf(buf, "HTTP/1.x 200 OK\r\nContent-Type: %s\r\nContent-Length: %d\r\nConnection: close\r\n\r\n",
					        mimeType.c_str(), outLength);
					cout << buf << endl;
					out = string(buf) + out;

				} else {

					fseek(requestedFile, stoi(structRanges->lowerRange), SEEK_SET);
					if (stoi(structRanges->upperRange) != -1) {
						for (int i = stoi(structRanges->lowerRange);
						     i <= stoi(structRanges->upperRange); i = ftell(requestedFile)) {
							c = fgetc(requestedFile);
							out += c;
						}
					} else {

						for (int i = stoi(structRanges->lowerRange); i < fileEnd; i = ftell(requestedFile)) {
							c = fgetc(requestedFile);
							out += c;
						}
					}
					int outLength = out.length();
					char buf[100];
					sprintf(buf, "HTTP/1.x 200 OK\r\nContent-Type: %s\r\nContent-Length: %d\r\nConnection: close\r\n\r\n",
					        mimeType.c_str(), outLength);
					cout << buf << endl;
					out = string(buf) + out;
				}
				fclose(requestedFile);
				const char* tmp = out.c_str();

				int sent = 0;
				cout << "schicke datei groesse: " << out.length() << endl;
				if ((sent = send(sd, tmp, out.length(), 0)) < 0) {
					cout << "Failed to send on SD " << sd << endl;
					logString += " Failed to send on SD ";
				} else {
					cout << sent << ": Bytes gesendet von: " << path << endl;
					logString += " Angefragte Datei wurde gesendet ";
				}


			/*
          * Behandlung von ganzen Verzeichnissen, die requestet werden
          */
			} else {

				DIR* dir = opendir(path);
				if (dir == nullptr) {
					logString += " Pfad nicht gefunden ";
					cout << "Pfad nicht gefunden: " << path << endl;
					string err("Pfad nicht gefunden");

					if (send(sd, err.c_str(), err.length(), MSG_NOSIGNAL) < 0) {
						cout << "Failed to send on SD " << sd << endl;

					}
				} else {
					string directoryListing(
						"<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<table><tr><td>Name</td><td>Laenge</td><td>Aenderungsdatum</td> </tr>");

					int gefunden = 0;
					struct dirent* dirFile;
					while ((dirFile = readdir(dir)) != NULL) {
						if (!strcmp(dirFile->d_name, "index.html")) {
							gefunden = 1;
							break;
						}
						struct stat fileStat;
						stat((pathstr + (string(dirFile->d_name))).c_str(), &fileStat);
						char buf[1000];
						sprintf(buf, "<tr><td>%s</td><td>%ld</td><td>%s</td> </tr>\n", dirFile->d_name, fileStat.st_size,
						        ctime(&fileStat.st_ctime));
						directoryListing.append(buf);
					}

					if (gefunden) {
						FILE* index;
						if ((index = fopen((pathstr + string("index.html")).c_str(), "r+")) == NULL) {
							cout << "Fehler beim Oeffnen der index.html" << endl;
						} else {
							char c = 'a';
							string out;
							if (!structRanges->isRangeSpecified) {

								while (c != EOF) {
									c = fgetc(index);
									out += c;
								}
								int outLength = out.length();
								char buf[100];
								sprintf(buf,
								        "HTTP/1.x 200 OK\r\nContent-Type: text/html\r\nContent-Length: %d\r\nConnection: close\r\n\r\n",
								        outLength);
								out = string(buf) + out;
								cout << buf << endl;

							} else {
								fseek(index, stoi(structRanges->lowerRange), SEEK_SET);
								if (stoi(structRanges->upperRange) != -1) {
									for (int i = stoi(structRanges->lowerRange); i <= stoi(structRanges->upperRange); i = ftell(index)) {
										c = fgetc(index);
										out += c;
									}
								} else {
									fseek(index, 0, SEEK_END);
									int fileEnd = ftell(index);
									fseek(index, stoi(structRanges->lowerRange), SEEK_SET);
									for (int i = stoi(structRanges->lowerRange); i < fileEnd; i = ftell(index)) {
										c = fgetc(index);
										out += c;
									}
								}
								int outLength = out.length();
								char buf[100];
								sprintf(buf,
								        "HTTP/1.x 206 Partial Content\r\nContent-Type: text/html\r\nContent-Length: %d\r\nConnection: close\r\n\r\n",
								        outLength);
								out = string(buf) + out;
								cout << buf << endl;
							}
							if (send(sd, out.c_str(), out.length(), 0) < 0) {
								cout << "Failed to send on SD " << sd << endl;
								logString += " Failed to send on SD ";
							} else {

								logString += " Index.html wurde gesendet ";
							}
						}
					} else {
						directoryListing.append(string("</table>\n</body>\n</html>"));
						int outLength = directoryListing.length();
						char buf[100];
						sprintf(buf,
						        "HTTP/1.x 200 OK\r\nContent-Type: text/html\r\nContent-Length: %d\r\nConnection: close\r\n\r\n",
						        outLength);
						directoryListing = string(buf) + directoryListing;
						cout << buf << endl;

						if (send(sd, directoryListing.c_str(), directoryListing.length(), 0) < 0) {
							cout << "Failed to send on SD " << sd << endl;
							logString += " Failed to send on SD ";
						} else {
							logString += " Directory Listing wurde gesendet ";
						}
					}
				}
			}
		}

		/*
       * Logging
       */
		cout << "Write to Log" << endl;
		logString += "\n\n";
		writeLogfile.lock();
		if ((logFile = fopen(logFileName, "a")) != NULL) {
			if ((fputs(logString.c_str(), logFile)) == EOF) {
				cout << "Fehler beim Schreiben in die Logdatei: " << logFileName << endl;
			}
			if ((fclose(logFile)) == EOF) {
				cout << "Fehler beim Schließen der Logdatei: " << logFileName << endl;
			}
		} else {
			cout << "Fehler beim Oeffnen der Logdatei: " << logFileName << endl;
		}
		writeLogfile.unlock();
	} while (keepAlive);

	cout << "Enter Mutex" << endl;
	// ====== Shutdown connection =========================================
	IOMutex.lock();
	cout << "Closing SD " << sd << endl;
	IOMutex.unlock();
	close(sd);

	thread->finished.exchange(true);
}


// ###### Main program ######################################################
int main(int argc, char** argv) {
	if (argc < 4) {
		cerr << "Usage: " << argv[0] << " [Port]" << " [Directory]" << " [Logfile]" << endl;
		exit(1);
	}
	const char* localService = argv[1];
	baseDir = argv[2];
	const char* logFile = argv[3];

	cout << "Port: " << localService << " Directory: " << baseDir << " Logfile: " << logFile << endl;


	// ====== Get local address (resolve hostname and service) ===============
	struct addrinfo* ainfo = NULL;
	struct addrinfo ainfohint;
	memset((char*) &ainfohint, 0, sizeof(ainfohint));
	ainfohint.ai_flags = AI_PASSIVE;
	ainfohint.ai_family = AF_UNSPEC;
	ainfohint.ai_socktype = SOCK_STREAM;
	ainfohint.ai_protocol = IPPROTO_TCP;
	int error = getaddrinfo(NULL, localService, &ainfohint, &ainfo);
	if (error != 0) {
		cerr << "ERROR: getaddrinfo() failed: " << gai_strerror(error) << endl;
		exit(1);
	}

	// ====== Convert local address to human-readable format =================
	char resolvedHost[NI_MAXHOST];
	char resolvedService[NI_MAXSERV];
	error = getnameinfo(ainfo->ai_addr, ainfo->ai_addrlen,
	                    (char*) &resolvedHost, sizeof(resolvedHost),
	                    (char*) &resolvedService, sizeof(resolvedService),
	                    NI_NUMERICHOST);
	if (error != 0) {
		cerr << "ERROR: getnameinfo() failed: " << gai_strerror(error) << endl;
		exit(1);
	}
	cout << "Binding to local host "
	     << resolvedHost << ", service " << resolvedService << " ..." << endl;

	// ====== Create socket of appropriate type ==============================
	int sd = socket(ainfo->ai_family, ainfo->ai_socktype, ainfo->ai_protocol);
	if (sd <= 0) {
		perror("Unable to create socket");
		exit(1);
	}

	// ====== Bind to local port =============================================
	if (bind(sd, ainfo->ai_addr, ainfo->ai_addrlen) < 0) {
		perror("bind() call failed");
		exit(1);
	}

	// ====== Turn socket into "listen" mode =================================
	if (listen(sd, 10) < 0) {
		perror("listen() call failed!");
	}


	// ====== Main loop ======================================================
	set<WorkerThread*> threadSet;

	installBreakDetector();
	while (!breakDetected()) {
		// ====== Wait for events =============================================
		pollfd pfd[1];
		pfd[0].fd = sd;
		pfd[0].events = POLLIN;
		int events = poll((pollfd*) &pfd, 1, -1);

		// ====== Handle events ===============================================
		if (events > 0) {


			// ====== Accept incoming connection ===============================
			sockaddr_storage remoteAddress;
			socklen_t remoteAddressLength = sizeof(remoteAddress);
			int newSD = accept(sd, (sockaddr*) &remoteAddress, &remoteAddressLength);
			struct sockaddr_in* pV4Addr = (struct sockaddr_in*) &remoteAddress;
			struct in_addr ipAddr = pV4Addr->sin_addr;
			if (newSD < 0) {
				perror("accept() call failed");
				break;
			}
			// ====== Perform application ======================================
			WorkerThread* newThread = new WorkerThread;
			assert(newThread != nullptr);
			newThread->finished = false;
			newThread->sd = newSD;
			newThread->thread = thread(performApplication, newThread, newSD, ipAddr, logFile);
			threadSet.insert(newThread);

			IOMutex.lock();
			cout << "Created thread for SD " << newThread->sd << endl;
			IOMutex.unlock();

			// ====== Garbage collection =======================================
			auto iterator = threadSet.begin();
			while (iterator != threadSet.end()) {
				WorkerThread* currentThread = *iterator;
				iterator++;

				if (currentThread->finished) {
					IOMutex.lock();
					cout << "Removing thread for SD " << currentThread->sd << endl;
					IOMutex.unlock();
					currentThread->thread.join();
					threadSet.erase(currentThread);
					delete currentThread;
				}
			}
		}
	}
	uninstallBreakDetector();


	// ====== Clean up =======================================================
	auto iterator = threadSet.begin();
	while (iterator != threadSet.end()) {
		WorkerThread* currentThread = *iterator;
		IOMutex.lock();
		cout << "Removing thread for SD " << currentThread->sd << endl;
		IOMutex.unlock();
		shutdown(currentThread->sd, SHUT_RDWR);   // Make sure the connection shut down!
		currentThread->thread.join();
		threadSet.erase(currentThread);
		delete currentThread;
		iterator = threadSet.begin();
	}
	freeaddrinfo(ainfo);
	close(sd);
	return 0;
}
