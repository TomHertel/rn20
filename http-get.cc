

/*
 * File Download via HTTP 1.1 GET Request
 * Copyright (C) 2020 by Thomas Dreibholz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact: dreibh@simula.no
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <iostream>

#include <unordered_map>
#include <optional>
#include <string>
#include <sstream>
#include <vector>

class Arguments {
public:
	static Arguments parse(int argc, const char** argv) {
		auto vec = std::vector<std::string>{};
		for (auto i = 1; i < argc; i++) {
			vec.emplace_back(argv[i]);
		}
		return parse(vec);
	}

	/*Geht alle Argumente durch und wenn es mit einem Minus startet wird daraus eine Flag gemacht ansonsten ist es ein Positionales Argument.
    Flag wird dann in die unordered_map gepackt.*/

	static Arguments parse(const std::vector<std::string>& args) {
		auto arguments = Arguments{};

		for (auto i = 0; i < args.size(); i++) {
			const auto& arg = args[i];
			if (arg.find("-") != 0) {
				arguments._arguments.emplace_back(arg);
			} else {
				// TODO: Handle index out bounds
				arguments._flags[arg.substr(1)] = args[i + 1];
				i++;
			}
		}

		return arguments;
	}

	/*Gibt die Argumente zurück*/

	const std::vector<std::string>& positional_arguments() const {
		return _arguments;
	}

	/*Holt eine Flag wenn er keine findet gibt er ein leeres Optional zurück anonsten den String.
    std::optional damit etwas leeres zurück gegeben wird und nicht ein nullpointer oder ähnliches.*/

	std::optional<std::string> get_flag(std::string name) const {
		const auto iter = _flags.find(name);
		if (iter == _flags.end()) return {};

		return iter->second;
	}

private:
	std::unordered_map<std::string, std::string> _flags{};
	std::vector<std::string> _arguments{};
};

struct SlowMotionFlag {
	size_t bytes;
	size_t sleepMilliseconds;
};

/*Parsed die eingabe für den die Slow motion in einen Flag um damit zu arbeiten wenn es keine 2 Elemente gibt gitb es eine Leeres Optional*/

std::optional<SlowMotionFlag> parse_slow_motion(const std::string& str) {
	const auto flag = SlowMotionFlag{};
	if (sscanf(str.c_str(), "%zu:%zu", &flag.bytes, &flag.sleepMilliseconds) != 2) {
		return {};
	}

	return flag;
}

// ###### Main program ######################################################
int main(const int argc, const char** argv) {
	const auto arguments = Arguments::parse(argc, argv);

	const auto& positional_arguments = arguments.positional_arguments();

	// ====== Check arguments ================================================

   /*3 Argumente werden erwartet somit wird danach geguckt*/

	if (positional_arguments.size() != 3) {
		std::cerr << "Usage: " << argv[0] << " [Remote Host] [Remote Service or Port] [Requested File]"
		          << std::endl;
		exit(1);
	}

	/*Flag wird geholt und Optional kann als Boolean gesehen werden somit wird dann geguckt ob etwas überhaupt drin ist.*/

	const auto range_flag = arguments.get_flag("r");
	std::string range = "0-";
	if (range_flag) {
		range = *range_flag;
	}

	/*Flag wird geholt und geparsed wegen dem Doppelpunkt*/

	const auto slow_motion_flag_str = arguments.get_flag("s");
	std::optional<SlowMotionFlag> slow_motion;
	if (slow_motion_flag_str) {
		slow_motion = parse_slow_motion(*slow_motion_flag_str);
		if (!slow_motion) {
			std::cout << "Invalid slow motion flag. Format: 'bytes:milliseconds'";
			return 1;
		}
	}

	const auto remoteHost = positional_arguments[0];
	const auto remoteService = positional_arguments[1];
	const auto requestedFile = positional_arguments[2];

	// ====== Get remote address (resolve hostname and service) ==============
	struct addrinfo* ainfo = NULL;
	struct addrinfo ainfohint;
	memset((char*) &ainfohint, 0, sizeof(ainfohint));
	// ainfohint.ai_flags    = 0;
	ainfohint.ai_family = PF_UNSPEC;
	ainfohint.ai_socktype = SOCK_STREAM;
	ainfohint.ai_protocol = IPPROTO_TCP;
	int error = getaddrinfo(remoteHost.c_str(), remoteService.c_str(), &ainfohint, &ainfo);
	if (error != 0) {
		std::cerr << "ERROR: getaddrinfo() failed: " << gai_strerror(error) << std::endl;
		exit(1);
	}

	// ====== Convert remote address to human-readable format ================
	char resolvedHost[NI_MAXHOST];
	char resolvedService[NI_MAXSERV];
	error = getnameinfo(ainfo->ai_addr, ainfo->ai_addrlen,
	                    (char*) &resolvedHost, sizeof(resolvedHost),
	                    (char*) &resolvedService, sizeof(resolvedService),
	                    NI_NUMERICHOST);
	if (error != 0) {
		std::cerr << "ERROR: getnameinfo() failed: " << gai_strerror(error) << std::endl;
		exit(1);
	}
	std::cout << "Connecting to remote host "
	          << resolvedHost << ", service " << resolvedService << " ..." << std::endl;

	// ====== Create socket of appropriate type ==============================
	int sd = socket(ainfo->ai_family, ainfo->ai_socktype, ainfo->ai_protocol);
	if (sd <= 0) {
		perror("Unable to create socket");
		exit(1);
	}

	// ====== Connect to remote node ==========================================
	if (connect(sd, ainfo->ai_addr, ainfo->ai_addrlen) < 0) {
		perror("connect() call failed");
		exit(1);
	}

	// ====== Request webpage =================================================
	std::cout << "Connected! Sending HTTP GET..." << std::endl;

   /*Wenn Slow motion gesetzt ist ersetllen wir und einen String Stream und senden diesen dann in der angegebenen Größe alle paar angegebenen Millisekunden raus.
    Nach jedem Senden wird der Teil der Gesendet wurde aus dem String gelöscht.*/

	if (slow_motion) {
		auto bufferStream = std::stringstream{};
		bufferStream << "GET " << requestedFile << " HTTP/1.0" << "\r\n";
		bufferStream << "Host: " << remoteHost << "\r\n";
		bufferStream << "Range: " << "bytes=" << range << "\r\n";
		bufferStream << "Connection: " << "close" << "\r\n";
		bufferStream << "\r\n";

		auto buffer = bufferStream.str();

		std::cout << "\x1b[37m" << buffer << "\x1b[0m";

		std::cout << "Trickling write";
		while (!buffer.empty()) {
			std::cout << ".";
			std::cout.flush();
			// TODO: CHeck if this is really needed
			auto to_write = std::min(slow_motion->bytes, buffer.size());
			if (write(sd, buffer.c_str(), to_write) < 0) {
				perror("write() call failed");
				exit(1);
			}
			buffer.erase(0, to_write);
			usleep(slow_motion->sleepMilliseconds * 1000);
		}

		std::cout << "\n";
	} else {
		char httpGet[256];
		snprintf((char*) &httpGet, sizeof(httpGet),
			//           "GET %s HTTP/1.0\r\nHost: %s\r\nConnection: close\r\n\r\n",
			       "GET %s HTTP/1.0\r\nHost: %s\r\nRange: bytes=%s\r\nConnection: close\r\n\r\n",
			       requestedFile.c_str(), remoteHost.c_str(), range.c_str());
		std::cout << "\x1b[37m" << httpGet << "\x1b[0m";
		if (write(sd, httpGet, strlen(httpGet)) < 0) {
			perror("write() call failed");
			exit(1);
		}
	}

	// ====== Receive reply ===================================================
	std::cout << "Request sent. Waiting for answer..." << std::endl;
	for (;;) {
		char data[256];

		ssize_t received = read(sd, (char*) &data, sizeof(data));
		if (received < 0) {
			perror("read() call failed");
			break;
		} else if (received == 0) {
			// Connection closed without error.
			break;
		} else {
			std::cout << "\x1b[34m";
			for (size_t i = 0; i < (size_t) received; i++) {
				if (isprint(data[i])) {
					// This works for ASCII characters. It may be useful to add
					// UTF-8 character handling here, in order to support other
					// characters!
					std::cout << data[i];
				} else if (data[i] == '\n') {
					std::cout << std::endl;
				} else if (data[i] == '\r') {
				} else {
					std::cout << '.';
				}
			}
			std::cout << "\x1b[0m";
		}
	}
	std::cout << std::endl;

	// ====== Clean up =======================================================
	freeaddrinfo(ainfo);
	close(sd);
	return 0;
}

